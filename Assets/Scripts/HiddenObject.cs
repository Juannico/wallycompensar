using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiddenObject : MonoBehaviour
{
    public int id;
    public bool isPressed;
    GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PressImage() 
    {
        isPressed = true;
        gameManager.TestPressedImage(id);
        gameObject.GetComponent<Button>().enabled = false;
    }
}
