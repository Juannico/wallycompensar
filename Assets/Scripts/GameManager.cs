using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject startScreen, gameScreen, finalScreen, gameInstructions;
    public AudioClip rightClip, wrongClip;
    //public string[] questionsArray;
    public List<string> questions;
    public TMP_Text timer, scoreText, finalScoreText, questionText, rightwrongText;
    public float startTime, currentTime;
    bool questionTimeRunning;
    int questionCounter, score, randomQuestion, questionLength;
    public List<GameObject> itemList;
    public List<bool> asked;
    GameObject[] itemArray;
    // Start is called before the first frame update
    void Start()
    {
        itemArray = GameObject.FindGameObjectsWithTag("item");
        questionLength = questions.Count;
        gameScreen.SetActive(false);
        gameInstructions.SetActive(false);
        rightwrongText.transform.parent.gameObject.SetActive(false);
        finalScreen.SetActive(false);
        currentTime = startTime;
        startScreen.SetActive(true);
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Puntaje: " + score;
        finalScoreText.text = "Puntaje final: " + score + "/" + questionLength;
        if (questionTimeRunning)
        {
            currentTime -= Time.deltaTime;
            timer.text = "Tiempo: " + currentTime.ToString("0");
            if (currentTime <= 0)
            {
                currentTime = 0;
                questionTimeRunning = false;
                finalScreen.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }

    public void Instructions()
    {
        gameInstructions.SetActive(true);
    }

    public void StartGame()
    {
        gameInstructions.SetActive(false);
        startScreen.SetActive(false);
        Time.timeScale = 1;
        gameScreen.SetActive(true);
        GenerateQuestion();
    }
    void GenerateQuestion()
    {
        questionTimeRunning = true;
        foreach (GameObject item in itemArray)
        {
            item.GetComponent<Button>().enabled = true;
            item.GetComponent<HiddenObject>().isPressed = false;
        }
        //empiezo ronda, voy gastando preguntas sin repetir hasta que me las acabe o se me acabe el tiempo, 
        //si se acaba el tiempo o acabo las preguntas, veo puntaje y reinicio juego y preguntas
        questionCounter++;
        if(itemList.Count > 0)
        {
            bool testBool = true;
            do
            {
                randomQuestion = Random.Range(0, questions.Count);
                if (!asked[randomQuestion])
                {
                    testBool = false;
                }
            }
            while (testBool);
            questionText.text = questions[randomQuestion];
        }
        if (questionCounter >= questions.Count)
        {
            finalScreen.SetActive(true);
        }

    }

    public void TestPressedImage(int id) 
    {
        print("id " + id);
        rightwrongText.transform.parent.gameObject.SetActive(true);
        StartCoroutine(TurnOffRight());
        if (id == randomQuestion)
        {
            rightwrongText.GetComponent<AudioSource>().clip = rightClip;
            rightwrongText.GetComponent<AudioSource>().Play();
            asked[randomQuestion] = true;
            rightwrongText.text = "Muy bien";
            score++;
            //correct
        }
        else
        {
            rightwrongText.GetComponent<AudioSource>().clip = wrongClip;
            rightwrongText.GetComponent<AudioSource>().Play();
            rightwrongText.text = "Incorrecto";
            //wrong
        }
    }
    IEnumerator TurnOffRight()
    {
        yield return new WaitForSeconds(2);
        rightwrongText.transform.parent.gameObject.SetActive(false);
        GenerateQuestion();
    }
    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

}
