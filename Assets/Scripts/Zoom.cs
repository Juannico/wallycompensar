using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Zoom : MonoBehaviour//, IPointerDownHandler, IPointerUpHandler
{
    //bool isDragging;
    bool zoomIn;
    public Transform[] magGlasses;
    //float sensitivity = 2f;
    ////Vector3 cameraPosition, mousePositionOnScreen, mousePosOnScreen1, camPos1, camDragBegin, camDragNext, mouseOnWorld;
    //public float minScale, maxScale;
    //float currentScale, scaleRate, temp, currentScaleY;
    //Vector3 startScale, startPos;
    // Start is called before the first frame update
    void Start()
    {
        //cameraPosition = Camera.main.transform.position;
        //mousePositionOnScreen = new Vector3();
        //mousePosOnScreen1 = new Vector3();
        //camPos1 = new Vector3();
        //mouseOnWorld = new Vector3();

        //startScale = transform.localScale;
        //startPos = transform.position;
        //currentScale = transform.localScale.x;
        //currentScaleY = transform.localScale.y;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (isDragging)
        {
            if(Input.touchCount == 2)
            {
                transform.localScale = new Vector3(currentScale, currentScaleY);
                float distance = Vector3.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
                if(temp > distance)
                {
                    if(currentScale < minScale)
                        return;
                    currentScale -= scaleRate * Time.deltaTime;
                }
                else if(temp < distance)
                {
                    if (currentScale > maxScale)
                        return;
                    currentScale += scaleRate * Time.deltaTime;
                }
                temp = distance;
            }
        }*/
    }

    /*public void OnPointerDown(PointerEventData eventData)
    {
        if(Input.touchCount == 2)
        {
            isDragging = true;
        }
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        isDragging = false;
    }*/
    public void ResetZoom(int magGlass)
    {
        if (zoomIn)
        {
            switch (magGlass)
            {
                case 1:
                    transform.position = magGlasses[1].position;
                    transform.localScale = magGlasses[1].localScale;
                    break;
                case 2:
                    transform.position = magGlasses[2].position;
                    transform.localScale = magGlasses[2].localScale;
                    break;
                case 3:
                    transform.position = magGlasses[3].position;
                    transform.localScale = magGlasses[3].localScale;
                    break;
                case 4:
                    transform.position = magGlasses[4].position;
                    transform.localScale = magGlasses[4].localScale;
                    break;
                case 5:
                    transform.position = magGlasses[5].position;
                    transform.localScale = magGlasses[5].localScale;
                    break;
            }
            zoomIn = false;
        }
        else
        {
            transform.position = magGlasses[0].position;
            transform.localScale = magGlasses[0].localScale;
            zoomIn = true;
            //transform.localScale = startScale;
            //transform.position = startPos;
        }
    }
}
